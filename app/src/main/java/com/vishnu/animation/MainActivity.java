package com.vishnu.animation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    ImageView boy;
    ImageView heart;
    ImageView girl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        boy=(ImageView)findViewById(R.id.imageView);
        heart=(ImageView)findViewById(R.id.imageView3);
        girl=(ImageView)findViewById(R.id.imageView2);
        Animation heart1=AnimationUtils.loadAnimation(getApplicationContext(),R.anim.bling);
        heart.startAnimation(heart1);
        heart.setVisibility(View.INVISIBLE);

        Animation animation= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.togerther);
        Animation boy1= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.boy);

        girl.startAnimation(animation);
        boy.startAnimation(boy1);

    }
}
